import time
import cv2
from base_camera import BaseCamera
import RPi.GPIO as GPIO
import time

# GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

# set GPIO Pins
TRIG = 18
ECHO = 24

GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)

class Camera(BaseCamera):
    """An emulated camera implementation that streams a repeated sequence of
    files 1.jpg, 2.jpg and 3.jpg at a rate of one frame per second."""
    #imgs = [open(f + '.jpg', 'rb').read() for f in ['1', '2', '3']]
    empty_img = open('empty.jpg', 'rb').read()

    @staticmethod
    def frames():
	camera = cv2.VideoCapture(0)
	distance = 0
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

	while True:
		print "Distance:",distance,"cm"
		GPIO.output(TRIG, False)
		read = False

		x = 0
		while (read == False and x < 25000) or (read == True and x < 8):
			x += 1
			if distance < 15 and distance > 5:
				read = True
				_, img = camera.read()
				yield cv2.imencode('.jpg', img)[1].tobytes()
			else:
				read = False
				yield Camera.empty_img

		GPIO.output(TRIG, True)
		time.sleep(0.00001)
		GPIO.output(TRIG, False)

		while GPIO.input(ECHO)==0:
		  pulse_start = time.time()

		while GPIO.input(ECHO)==1:
		  pulse_end = time.time()

		pulse_duration = pulse_end - pulse_start

		distance = pulse_duration * 17150

		distance = round(distance, 2)

			
